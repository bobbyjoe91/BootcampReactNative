// No. 1
console.log('No. 1');

console.log('LOOPING PERTAMA');
let counter = 2;
while(counter <= 20) {
	console.log(`${counter} - I love coding`);
	counter += 2;
}

counter -= 2;
console.log('LOOPING KEDUA');
while(counter >= 2) {
	console.log(`${counter} - I will become a mobile developer`);
	counter -= 2;
}

// No. 2
console.log('\nNo. 2');

let message;
for(let i=1; i <= 20; i++) {
	if(i%2 == 1) { // jika i adalah bilangan ganjil
		if(i%3 == 0) { // jika i ganjil dan kelipatan 3
			message = 'I love coding';
		} else {
			message = 'Santai';
		}
	} else { // jika i adalah bilangan genap
		message = 'Berkualitas';
	}

	console.log(`${i} - ${message}`);
}

// No. 3
console.log('\nNo. 3');

let baris = 4;
let kolom = 8;
let barisanPagar = '';

for(let i=0; i < baris; i++) {
	barisanPagar = '';
	for(let j=0; j < kolom; j++) {
		barisanPagar += '#';
	}

	console.log(barisanPagar);
}


// No. 4
console.log('\nNo. 4');

let tinggi = 7;
let alas = 7;

for(let i=0; i < tinggi; i++) {
	barisanPagar = '';
	for(let j=0; j < alas; j++) {
		if(j < i+1) {
			barisanPagar += '#';
		} else break;
	}

	console.log(barisanPagar);
}


// No. 5
console.log('\nNo. 5');

baris = 8;
kolom = 8;

for(let i=0; i < baris; i++) {
	barisanPagar = '';
	for(let j=0; j < kolom; j++) {
		if((i+j)%2 == 0) {
			barisanPagar += ' ';
		} else {
			barisanPagar += '#';
		}
	}

	console.log(barisanPagar);
}
