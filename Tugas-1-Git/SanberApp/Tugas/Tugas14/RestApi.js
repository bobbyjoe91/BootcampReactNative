import React, { useState } from 'react';
import { Text, View,
    StyleSheet, 
    TextInput,
    Button, 
    ScrollView, Dimensions, TouchableOpacity,
    Platform, Alert } from 'react-native';
import { useEffect } from 'react/cjs/react.development';
import Axios from 'axios';
import { useScrollToTop } from '@react-navigation/native';

function Items(props) {
    let internal = {
        activity: {
            alignSelf: 'flex-start',
            marginBottom: 20,
            width: Dimensions.get('window').width - 47,
        },
        innerActivity: {
            flexDirection: 'row',
        },
        text: {
            width: Dimensions.get('window').width - 60, 
        }
    };

    return(
        <View style={internal.activity}>
            <View style={internal.innerActivity}>
                <View style={internal.text}>
                    <Text>Aktivitas</Text>
                    <TouchableOpacity>
                        <Text style={{fontSize: 20}} onPress={props.onPress}>
                            {props.activityNote}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text onPress={props.onDelete}>X</Text>
                </View>
            </View>
        </View>
    );
}

// 10.0.2.2:8081
export default function RestApi() {
    const [note, setNote] = useState('');
    const [notes, setNotes] = useState([]);
    const [saveButton, setSaveButton] = useState('Simpan');
    const [selectedNote, setSelectedNote] = useState({});

    const HOST = Platform.OS == 'ios' ? 'localhost' : '10.0.2.2';
    const PORT = '4200';

    // create and update
    async function submit() {
        const data = {
            note,
        };
        if(saveButton == 'Simpan') {
            console.log('================================');
            try {
                const res = await Axios.post(`http://${HOST}:${PORT}/notes`, data);
                setNote('');
                retrieve(); // read after post
                console.log(res.status);
                console.log(res.data);
            } catch(e) {
                console.log(e.response.status);
                throw new Error(e);
            }
        } else if(saveButton == 'Edit') {
            await Axios.put(`http://${HOST}:${PORT}/notes/${selectedNote.id}`, data);
            
            setNote('') // empty textbox
            retrieve(); // read after edit

            setSaveButton('Simpan');
        }
    }

    // read
    async function retrieve() {
        const res = await Axios.get(`http://${HOST}:${PORT}/notes`);
        setNotes(res.data)
        console.log(res);
    }
    useEffect(() => {
        retrieve();
    }, []);

    // update
    async function selectNote(note) {
        console.log(note);
        setNote(note.note);
        setSelectedNote(note);
        setSaveButton('Edit');
    }

    // delete
    function deleteNote(note) {
        Alert.alert('Peringatan', 'Anda yakin akan menghapus catatan ini?',
        [
            {
                text: 'Tidak',
                onPress: () => {}
            }, 
            {
                text: 'Ya',
                onPress: async () => {
                    await Axios.delete(`http://${HOST}:${PORT}/notes/${note.id}`);
                    retrieve();
                }
            }
        ]);
    }

    return(
        <View style={styles.container}>
            <Text>Tulis aktivitas</Text>
            <TextInput style={styles.input} 
                placeholder="Bersepeda ke pasar" 
                value={note} 
                onChangeText={(value) => setNote(value)}
            />
            <Button title={saveButton} onPress={submit} />

            <View style={{width: 200, borderWidth: 1, marginVertical: 10, backgroundColor: 'black'}} />
            
            <ScrollView>
                {notes.map(note => {
                    return (
                    <Items 
                        key={note.id} 
                        activityNote={note.note}
                        onPress={() => selectNote(note)}
                        onDelete={() => deleteNote(note)}
                    />
                    );
                })}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 150
    },
    textTitle: {
        textAlign: 'center',
        marginBottom: 20,
    },
    line: {
        height: 2,
        backgroundColor: 'black',
        marginVertical: 20
    },
    input: {
        height: 50,
        alignSelf: 'stretch',
        fontSize: 20,
        borderWidth: 1,
        marginBottom: 12,
        borderRadius: 20,
        paddingHorizontal: 18
    },
});