import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList } from 'react-native'
import { Data } from './data'

export default function Home({route, navigation}) {
    console.log('==>', route)
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };

    const updateHarga = (price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }

    const singleItem = ({ item }) => {
        const singleStyle = StyleSheet.create({
            container: {
                margin: 10,
                padding: 10,
                borderWidth: 1,
                borderRadius: 10,
                alignItems: 'center'
            },
            title: {
                fontWeight: 'bold',
            },
            type: {
                color: 'orange',
            },
            price: {
                color: 'green',
                fontSize: 17,
                margin: 5,
            }
        });

        return(
        <View style={singleStyle.container}>
            <Image source={item.image} style={{width: 150, height: 100}}/>
            <Text style={singleStyle.title}>{item.title}</Text>
            <Text style={singleStyle.type}>({item.type})</Text>
            <Text style={singleStyle.price}>Rp{item.harga}</Text>


            <Button title="Beli" onPress={() => updateHarga(Number(item.harga))}/>
        </View>
        );
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                    
                    {totalPrice > 0 ? <Text style={styles.reset} onPress={() => {setTotalPrice(0)}}>Reset</Text> : null}
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
            {/* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->

            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */
             
             }
             <FlatList 
                data={Data}
                renderItem={singleItem}
                keyExtractor={item => item.id}
                numColumns={2}
                horizontal={false}
             />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    reset: {
        color: 'red',
        marginTop: 10,
    }
})
