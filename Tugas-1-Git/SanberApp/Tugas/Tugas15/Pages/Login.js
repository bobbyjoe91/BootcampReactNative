import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default function Login(props) {
    return (
        <View>
            <Text>Login</Text>
            <Button title="Home Screen" onPress={() => props.navigation.navigate("AppDrawer", 
                {
                    screen: 'App', 
                    params: {
                        screen: 'AboutScreen'
                    }
                }
            )} />
        </View>
    )
}

const styles = StyleSheet.create({})
