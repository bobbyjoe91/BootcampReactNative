import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Button, 
    TextInput,
    Image 
} from 'react-native';

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
    }

    toAboutScreen() {
        this.props.navigation.navigate('About');
    }

    render() {
        return(
            <View style={styles.container}>
                <Image 
                    source={require('./assets/sanberLogo.png')}
                    style={styles.logo}
                />
                <View style={styles}>
                    <TextInput style={styles.textBox} placeholder="Username"/>
                    <TextInput
                        style={styles.textBox} 
                        placeholder="Password"
                        secureTextEntry={true}
                    />
                </View>
                <View style={{marginHorizontal: 150}}>
                    <Button 
                        title="Login"
                        onPress={() => this.toAboutScreen()}
                    />
                </View>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom: 140,
    },
    textBox: {
        height: 50, 
        marginVertical: 10,
        marginHorizontal: 40,
        textAlign: 'center',
        fontSize: 20,

        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30
    },
    logo: {
        alignSelf: 'center',
        marginVertical: 50
    },
});