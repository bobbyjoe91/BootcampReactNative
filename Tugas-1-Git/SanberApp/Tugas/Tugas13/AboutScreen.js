import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export default class AboutScreen extends Component {
    constructor(props) {
        super(props);
    }

    toLoginScreen() {
        this.props.navigation.navigate('Login');
    }

    render() {
        return(
            <View>
                <View style={styles.profile}>
                    <Image 
                        source={require('./assets/profilePic.png')}
                        style={styles.profileDetail}
                    />
                    <View style={styles.profileDetail}>
                        <Text style={{fontSize: 46}}>Full name</Text>
                        <Text style={{fontSize: 24}}>Username</Text>
                        <Text style={{fontSize: 14, marginTop: 5}}>Bio</Text>
                    </View>
                </View>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    profile: {
        flexDirection: 'row',
        marginTop: 40,
        justifyContent: 'center'
    },
    profileDetail: {
        marginHorizontal: 15,
    },
});