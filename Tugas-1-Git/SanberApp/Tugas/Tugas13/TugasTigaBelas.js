import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { create } from 'react-test-renderer';

const Stack = createStackNavigator();

export default class TugasTigaBelas extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Login" component={LoginScreen} options={{ title: 'SanberApp' }}/>
                    <Stack.Screen name="About" component={AboutScreen}/>
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

let styles = StyleSheet.create({

});