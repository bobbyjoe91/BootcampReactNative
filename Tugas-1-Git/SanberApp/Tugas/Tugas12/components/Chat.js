import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

export default function Chat(props) {
    const item = props.chatData;

    return(
        // return single chat item
        <>
            <TouchableOpacity style={styles.messageContainer}>
                <View style={{flexDirection: 'row'}}>
                    <Image 
                        style={{height: 50, width: 50, borderRadius: 25}}
                        source={item.image}
                    />
                    <View styles={styles.nameMessage}>
                        <Text>{item.name}</Text>
                        <Text>{item.message}</Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Image 
                            style={{height: 8, width: 12, marginRight: 5}}
                            source={require('../assets/check.png')}
                        />
                        <Text>{item.time}</Text>
                    </View>
                    <View style={{
                        backgroundColor: '#4ECC5E',
                        height: 24,
                        width: 24,
                        borderRadius: 12,
                        alignItems: 'center'
                    }}>
                        <Text style={{color: 'white'}}>{item.totalMessage}</Text>
                    </View>
                </View>
            </TouchableOpacity>
            <View style={{borderBottomWidth: 1, borderBottomColor:'#A8AAAB'}}></View>
        </>
    );
}

const styles = StyleSheet.create({
    messageContainer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingTop: 27,
        paddingHorizontal: 24,
        paddingVertical: 24,
        justifyContent: 'space-between'
    },
    nameMessage: {
        paddingLeft: 15
    },
});