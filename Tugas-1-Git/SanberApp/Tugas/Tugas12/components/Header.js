import React from 'react';
import { Platform } from 'react-native';
import { View, Text, Image, StyleSheet } from 'react-native';

export default function Header() {
    return(
        <View style={styles.header}>
            <View style={styles.subHeader}>
                <View styles={styles.subContentHeader}>
                    <Image 
                        source={require('../assets/drawer.png')}
                        style={styles.avatarDrawer}
                    />
                    <Text style={styles.titleName}>Telegram</Text>
                </View>
                <View>
                    <Image 
                        source={require('../assets/search.png')}
                        style={styles.avatarDrawer} 
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        height: Platform.OS == 'android' ? 50 : 100,
        backgroundColor: '#517DA2',
        justifyContent: 'center'
    },
    subHeader: {
        flexDirection: 'row',
        alignContent: 'center',
        paddingHorizontal: 18,
        justifyContent: 'space-between',
        marginTop: Platform.OS == 'android' ? 0 : 40
    },
    subContentHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatarDrawer: {
        height: 15,
        width: 15,
        marginTop: 10
    },
    titleName: {
        color: 'white',
        fontSize: 18,
        paddingLeft: 20,
        marginBottom: 21
    }
});