import React from 'react';
import { FlatList } from 'react-native';

import Chat from './Chat';
import { Data } from '../dummy';

export default function Conversations() {
    return(
        <FlatList 
            data={Data}
            keyExtractor={(item) => item.id}
            renderItem={({item}) => {
                return(
                    <Chat chatData={item}/>
                );
            }}
        />
    );
}