import React, { useEffect } from 'react';
import { View, Image, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';

import Header from './components/Header';
import Conversations from './components/Conversations';
import { Data } from './dummy';

const data = Data;

function Telegram() {
    useEffect(() => {
        console.log(data);
    }, [])

    return(
        <View style={styles.container}>
            <Header />
            <View style={styles.content}>
                {/* Contents are here */}
                <SafeAreaView>
                    <Conversations />
                </SafeAreaView>

                {/* Add chat (pencil) button */}
                <TouchableOpacity style={{paddingRight: 24, marginTop: 280}}>
                    <View style={styles.addChat}>
                        <Image 
                            style={styles.avatarPen}
                            source={require('./assets/pencil.png')}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    addChat: {
        height: 45,
        width: 45,
        borderRadius: 45/2,
        backgroundColor: '#4EA4EA',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end'
    },
    avatarPen: {
        height: 18,
        width: 18
    },
});

export default Telegram;