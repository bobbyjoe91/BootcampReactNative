/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, ScrollView } from 'react-native';

import Telegram from './Tugas/Tugas12/Telegram';
import TugasTigaBelas from './Tugas/Tugas13/TugasTigaBelas';
import RestApi from './Tugas/Tugas14/RestApi';
import TugasNav from './Tugas/Tugas15/TugasNav';
import Quiz3 from './Tugas/Quiz3/index';

let tugasKe = 16;

const App: () => React$Node = () => {
  let components = {
    12: <Telegram />,
    13: <TugasTigaBelas />,
    14: <RestApi />,
    15: <TugasNav />,
    16: <Quiz3 />,
  };

  return (
    <>
      { components[tugasKe] }
    </>
  );
};

export default App;
