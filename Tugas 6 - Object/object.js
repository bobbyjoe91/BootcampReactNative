// No. 1
console.log('No. 1');
function arrayToObject(arr) {
    let identity = {}, personalData, key;
    let currYear = new Date().getFullYear();

    for(let i = 0; i < arr.length; i++) {
        personalData = arr[i];
        key = `${i+1}. ${personalData[0]} ${personalData[1]}`
        identity[key] = {
            firstName: personalData[0],
            lastName: personalData[1],
            gender: personalData[2],
            age: personalData[3] <= currYear ? currYear-personalData[3] : 'Invalid Birth Year'
        };

        console.log(`${key}:`, identity[key]);
    }

    return identity;
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];

arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);

// No. 2
console.log('\nNo. 2');

// set variabel global shoppingItems
let shoppingItems = [
    { name: 'Sepatu Stacattu', price: 1500000 },
    { name: 'Baju Zoro', price: 500000 },
    { name: 'Baju H&N', price: 250000 },
    { name: 'Sweater Uniklooh', price: 175000 },
    { name: 'Casing Handphone', price: 50000 }
];

function shoppingTime(memberId, money) {
    if(memberId) {
        if(money >= 50000) {
            let receipt = { 
                memberId: memberId,
                money: money, 
                listPurchased: [], 
                changeMoney: 0 
            };

            for(let item of shoppingItems) {
                if(item.price <= money) {
                    receipt.listPurchased.push(item.name);
                    money -= item.price;
                }
            }

            receipt.changeMoney = money;
            return receipt;
        }

        return 'Mohon maaf, uang tidak cukup';
    }

    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// No. 3
console.log('\nNo. 3');

function naikAngkot(arrPenumpang) {
    const HARGA_SATUAN = 2000;
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'], catatan = [];
    let jarak, ongkos, asal, tujuan, namaPenumpang;

    for(let penumpang of arrPenumpang) {
        namaPenumpang = penumpang[0];
        asal = penumpang[1];
        tujuan = penumpang[2];

        // hitung jarak dengan menghitung selisih kode ASCII asal dan tujuan
        jarak = Math.abs(tujuan.charCodeAt() - asal.charCodeAt());

        catatan.push({
            penumpang: namaPenumpang,
            naikDari: asal,
            tujuan: tujuan,
            bayar: jarak * HARGA_SATUAN
        });
    }

    return catatan;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
