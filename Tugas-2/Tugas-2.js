/* Tugas String */
// No. 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`);

// No. 2
var sentence = "I am going to be React Native Developer";

// var sentenceSplitted = sentence.split(' ');

var exampleFirstWord = sentence[0]; 
var exampleSecondWord = sentence[2] + sentence[3]; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12]; 
var fifthWord = sentence[14] + sentence[15]; 
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]; 
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; 
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord); 

// No. 3
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

// No. 4
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 

/* Tugas Conditional */
// If-else
var nama = 'John';
var peran = '';

if(nama) {
	if(peran) {
		sayHello = `Halo ${peran} ${nama}`;
		switch(peran) {
			case 'Penyihir': { console.log(`${sayHello}, kamu dapat melihat siapa yang menjadi werewolf!`); break; }
			case 'Guard': { console.log(`${sayHello}, kamu akan membantu melindungi temanmu dari serangan werewolf.`); break; }
			case 'Werewolf': { console.log(`${sayHello}, Kamu akan memakan mangsa setiap malam!`); break; }
			default: { console.log('Maaf, peran tidak valid. Silahkan pilih \'Penyihir\', \'Guard\', atau \'Werewolf\''); }
		}
	} else {
		// jika nama terisi dan peran tidak terisi
		console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
	}
} else {
	// jika nama tidak diisi
	console.log('Nama harus diisi!')
}

// Switch-case
var hari = 21;
var bulan = 1;
var tahun = 1945;
var isDateValid = true;

switch(bulan) {
	case 1: { bulan = 'Januari'; break; }
	case 2: { bulan = 'Februari'; break; }
	case 3: { bulan = 'Maret'; break; }
	case 4: { bulan = 'April'; break; }
	case 5: { bulan = 'Mei'; break; }
	case 6: { bulan = 'Juni'; break; }
	case 7: { bulan = 'Juli'; break; }
	case 8: { bulan = 'Agustus'; break; }
	case 9: { bulan = 'September'; break; }
	case 10: { bulan = 'Oktober'; break; }
	case 11: { bulan = 'November'; break; }
	case 12: { bulan = 'Desember'; break; }
	default: { 
		console.log('Silahkan masukkan angka 1-12'); isDateValid = false
	}
}

if(isDateValid) {
	var tanggal = `${hari} ${bulan} ${tahun}`;
	console.log(tanggal);
}