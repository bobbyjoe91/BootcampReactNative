// No. 1
console.log('No. 1');

// parent -> Release 0
class Animal {
	constructor(name) {
		this.name = name;
		this.legs = 4;
		this.coldBlooded = false;
	}
}

let sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.coldBlooded);

// inheritance -> Release 1
class Ape extends Animal {
	constructor(name) {
		super(name);
		this.legs = 2;
	}

	yell() {
		console.log('Auooo');
	}
}

class Frog extends Animal {
	constructor(name) {
		super(name);
	}

	jump() {
		console.log('hop hop');
	}
}

let sungokong = new Ape('kera sakti');
sungokong.yell();

let kodok = new Frog('buduk');
kodok.jump();


// No. 2
console.log('\nNo. 2');

class Clock {
	constructor({ template }) {
		this.template = template;
		this.timer;
	}

	render() {
    	var date = new Date();

    	var hours = date.getHours();
    	if (hours < 10) hours = '0' + hours;

    	var mins = date.getMinutes();
    	if (mins < 10) mins = '0' + mins;

    	var secs = date.getSeconds();
    	if (secs < 10) secs = '0' + secs;

	    var output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);

    	console.log(output);
  	}

	start() {
		this.render();
		this.timer = setInterval(this.render.bind(this), 1000);
	}

	stop() {
		clearInterval(this.timer);
	}
}

let clock = new Clock({template: 'h:m:s'});
clock.start();