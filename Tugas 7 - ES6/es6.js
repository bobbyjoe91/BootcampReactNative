/* No. 1 | Mengubah fungsi menjadi fungsi arrow */
console.log('No. 1');

const golden = () => {
  console.log("this is golden!!")
}
 
golden()

/* No. 2 | Sederhanakan menjadi Object literal di ES6 */
console.log('\nNo. 2');

const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(`${firstName} ${lastName}`)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()

/* No. 3 | Destructuring */
console.log('\nNo. 3')

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
//dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

const { firstName, lastName, destination, occupation } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

/* No. 4 | Array Spreading */
console.log('\nNo. 4')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];

//Driver Code
console.log(combined)

/* No. 5 | Template Literals */
console.log('\nNo. 5')

const planet = "earth"
const view = "glass"
let before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 