// No. 1
console.log('No. 1');

function range(startNum=null, finishNum=null) {
	if(startNum && finishNum) {
		let result = [];
		let step = startNum < finishNum ? 1 : -1;
		
		let i = startNum;
		while(i != finishNum+step) {
			result.push(i);
			i += step;
		}

		return result;
	}
	
	return -1;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// No. 2
console.log('\nNo. 2');

function comparator(startNum, finishNum, currentNumber) {
	if(startNum <= finishNum) { // jika urutan menaik
		// hentikan looping jika currentNumber > finishNum
		return currentNumber > finishNum ? false : true;
	}
	
	return currentNumber < finishNum ? false : true;
}

function rangeWithStep(startNum=null, finishNum=null, step=1) {
	if(startNum && finishNum) {
		let result = [];
		step = startNum < finishNum ? step : -step;
	
		let i = startNum;
		while(comparator(startNum, finishNum, i)) {
			result.push(i);
			i += step;
		}

		return result;
	}
	
	return -1;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// No. 3
console.log('\nNo. 3');

function sum(startNum=null, finishNum=null, step=1) {
	if(startNum && finishNum) {
		let result = 0;
		step = startNum < finishNum ? step : -step;
	
		let i = startNum;
		while(comparator(startNum, finishNum, i)) {
			result += i;
			i += step;
		}

		return result;
	} else if(startNum) {
		return startNum;
	}

	return 0;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// No. 4
console.log('\nNo. 4');

function dataHandling(data) {
	let result = '';
	let datum;
	for(let i=0; i < data.length; i++) {
		datum = data[i];
		result += `Nomor ID: ${datum[0]}\nNama Lengkap: ${datum[1]}\nTTL: ${datum[2]} ${datum[3]}\nHobi: ${datum[4]}`;

		if(i < data.length-1) result += '\n\n'; 
	}

	return result
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log(dataHandling(input));

// No. 5
console.log('\nNo. 5');

function balikKata(string) {
	let result = '';
	for(let i=string.length-1; i > -1; i--) {
		result += string[i];
	}

	return result;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// No. 6
console.log('\nNo. 6');

function convertNumeralMonth(monthNum) {
	let bulan;
	switch(monthNum) {
		case 1: { bulan = 'Januari'; break; }
		case 2: { bulan = 'Februari'; break; }
		case 3: { bulan = 'Maret'; break; }
		case 4: { bulan = 'April'; break; }
		case 5: { bulan = 'Mei'; break; }
		case 6: { bulan = 'Juni'; break; }
		case 7: { bulan = 'Juli'; break; }
		case 8: { bulan = 'Agustus'; break; }
		case 9: { bulan = 'September'; break; }
		case 10: { bulan = 'Oktober'; break; }
		case 11: { bulan = 'November'; break; }
		case 12: { bulan = 'Desember'; break; }
		default: { bulan = null; }
	}

	return bulan;
}

function dataHandling2(data) {
	// step 1, splice
	data[1] += 'Elsharawy';
	data[2] = 'Provinsi ' + data[2];
	data.splice(4, 1, "Pria", "SMA International Metro");
	console.log(data);

	// step 2, split
	let date = data[3];
	let dateSplitted = date.split('/')
	let monthNumber = parseInt(dateSplitted[1]);
	console.log(convertNumeralMonth(monthNumber));

	// step 3, sort
	let dateSplitted2 = [...dateSplitted].map((x) => {
		return parseInt(x);
	});
	let stepTwoResult = dateSplitted2.sort().map((x) => {
		return x < 10 ? '0'+x.toString() : x.toString();
	});
	console.log(stepTwoResult);

	// step 4, join
	console.log(dateSplitted.join('-'))

	// step 5, slice
	console.log(data[1].slice(0, 15));

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);