let readBooksPromise = require('./promise.js');

let books = [
	{ name: 'LOTR', timeSpent: 3000 }, 
	{ name: 'Fidas', timeSpent: 2000 }, 
	{ name: 'Kalkulus', timeSpent: 4000 }
];

let time = 10000;

readBooksPromise(time, books[0])
	.then((remainingTime) => { return readBooksPromise(remainingTime, books[1]) })
	.then((remainingTime2) => { return readBooksPromise(remainingTime2, books[2]) })
	.catch((timeDeficit) => {
		console.log(`Saya kekurangan waktu ${Math.abs(timeDeficit)}`);
	});

// async function readAllBooks(time) {
// 	try {
// 		let remainingTime = await readBooksPromise(time, books[0]);
// 		let remainingTime2 = await readBooksPromise(remainingTime, books[1]);
// 		let remainingTime3 = await readBooksPromise(remainingTime2, books[2]);
// 	} catch(timeDeficit) {
// 		console.log(`Saya kekurangan waktu ${Math.abs(timeDeficit)}`);
// 	}
// }

// readAllBooks(10000);