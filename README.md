# BootcampReactNative

### Catatan untuk pengoreksi
Untuk mengoreksi Tugas 12 hingga 15 dan Quiz 3, silahkan ubah nilai variabel 'tugasKe' pada Tugas-1-Git/SanberApp/App.js.
Nilai yang valid berada pada rentang 12-16. 

```
let tugasKe = 16; //ubah di sini

const App: () => React$Node = () => {
  let components = {
    12: <Telegram />,
    13: <TugasTigaBelas />,
    14: <RestApi />,
    15: <TugasNav />,
    16: <Quiz3 />,
  };

....
```
